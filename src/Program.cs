﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
/// <summary>
/// @Author: Balwinder Sodhi
/// </summary>
namespace io.github.bsodhi.ExcelUtils
{
    /// <summary>
    /// A quick and dirty testing driver. Hopefully, it should also
    /// serve as an example for how to use ExcelHelper class.
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            String tpl1 = "TestData.xlsx", tpl2 = "TestDataForDict.xlsx";
            
            // Run object input case
            using(ExcelHelper eh = new ExcelHelper(tpl1))
            {
                var data = MyData.Dummy(10);
                eh.PopulateSheet(data, tpl1.Replace(".xlsx", "_out.xlsx"));
            }

            // Run dictionary input case
            using(ExcelHelper eh = new ExcelHelper(tpl2))
            {
                var data = new { Data = GetDummyDataAsDict() };
                eh.PopulateSheet(data, tpl2.Replace(".xlsx", "_out.xlsx"));
            }

        }

        private static List<IDictionary<string, object>> GetDummyDataAsDict()
        {
            var rows = new List<IDictionary<string, object>>();
            for (int i = 0; i < 10; i++)
            {
                IDictionary<string, object> r = new Dictionary<string, object>();
                for (int j = 0; j < 4; j++)
                {
                    r.Add("MYK_" + j, "VAL_" + j + "_" + i);
                }
                rows.Add(r);
            }
            return rows;
        }

    }
    #region Dummy data
    class MyData
    {
        public int Year { get; set; }
        public string Info { get; set; }
        public Order Order { get; set; }
        public List<Student> Students { get; set; } = new List<Student>();
        public List<Course> Courses { get; set; } = new List<Course>();

        public static MyData Dummy(int s)
        {
            MyData d = new MyData();
            d.Year = 2012;
            d.Info = "TEST-" + s;
            d.Order = Order.Dummy(s);
            for (int i = 0; i < s; i++)
            {
                d.Students.Add(new Student(10 + s, "M", "SN-" + i));
                d.Courses.Add(new Course(s + 2000, "CN-" + i + "-" + s));
            }
            return d;
        }
    }

    class OrderItem
    {
        public string ItemName { get; set; }
        public decimal Rate { get; set; }
        public decimal Qty { get; set; }
    }
    class Order
    {
        public int OrderNo { get; set; }
        public List<OrderItem> Items { get; set; } = new List<OrderItem>();
        public static Order Dummy(int ic)
        {
            Order o = new Order();
            o.OrderNo = 123;
            for (int i = 0; i < ic; i++)
            {
                OrderItem oi = new OrderItem();
                oi.ItemName = "ITEM-" + i;
                oi.Qty = (decimal)(i + 2.34);
                oi.Rate = (decimal)(i + 1.6);
                o.Items.Add(oi);
            }
            return o;
        }
    }

    class Student
    {
        public int Age { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public Student(int age, string gender, string name)
        {
            Age = age;
            Gender = gender;
            Name = name;
        }
    }

    class Course
    {
        public int YearStarted { get; set; }
        public string Name { get; set; }
        public Course(int ys, string name)
        {
            Name = name;
            YearStarted = ys;
        }
    }

    #endregion
}
