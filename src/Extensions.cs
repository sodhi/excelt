using System;
using System.Collections.Generic;
using System.Reflection;
namespace io.github.bsodhi.ExcelUtils
{

    /// <summary>
    /// Implements extension methods for various uses.    
    /// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/how-to-implement-and-call-a-custom-extension-method
    /// 
    /// @author Balwinder Sodhi
    /// </summary>
    public static class Extensions
    {

        /// <summary>
        /// Checks whether the supplied object is a dictionary
        /// of type IDictionary<string, object> or
        /// Dictionary<string, object>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static bool IsDictionary(Object obj)
        {
            int yes = 0;
            try
            {
                var x = (IDictionary<string, object>)obj;
                yes++;
            }
            catch (Exception) { }
            try
            {
                var x = (Dictionary<string, object>)obj;
                yes++;
            }
            catch (Exception) { }

            return yes > 0;
        }

        /// <summary>
        /// Gets the value of a property on the object.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propName">Nested properties can be passed 
        /// using dot notation. For example, foo.bar.baz</param>
        /// <returns></returns>
        public static Object PropertyValue(this Object obj, String propName)
        {
            foreach (String part in propName.Split('.'))
            {
                if (obj == null) { break; }

                Type type = obj.GetType();
                if (IsDictionary(obj))
                {
                    var dict = (IDictionary<string, object>)obj;
                    obj = dict[part];
                }
                else
                {
                    PropertyInfo info = type.GetProperty(part);
                    if (info == null) { return null; }
                    obj = info.GetValue(obj, null);
                }
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = PropertyValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }
    }
}