using System;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using log4net;
using System.Collections;
using System.Text;
using System.Drawing;

namespace io.github.bsodhi.ExcelUtils
{
    /// <summary>
    /// Holds the repeater expressions information found in
    /// the template Excel file.
    /// 
    /// @author Balwinder Sodhi
    /// </summary>
    public class RepeatExp
    {
        public int RowsAdded { get; set; } = 0;
        public int ColumnsAdded { get; set; } = 0;
        /// <summary>
        /// Typically it contains three entries with keys as follows:
        /// "prop" -- For holding property full name. E.g., Order.TotalAmout
        /// "type" -- Type of the repeater, e.g., row or col.
        /// "range" -- Cell range containing repeater specs. E.g., A1:A12
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> Exp { get; set; } = new List<Dictionary<string, string>>();
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Added rows {0}, columns {1}. Exp: ",
            RowsAdded, ColumnsAdded));
            foreach (var e in Exp)
            {
                sb.Append("[");
                foreach (var kvp in e)
                {
                    sb.AppendFormat("({0}, {1})", kvp.Key, kvp.Value);
                }
                sb.Append("], ");
            }
            return sb.ToString();
        }
    }

    /// <summary>
    /// Helper class for filling in the Excel file based template.
    /// 
    /// @author Balwinder Sodhi
    /// </summary>
    public class ExcelHelper : IDisposable
    {
        public const string ROW = "row";
        public const string COL = "col";
        public const string NEW_DATA_SHEET = "Data";
        public const string KEY_PROP = "prop";
        public const string KEY_TYPE = "type";
        public const string KEY_RANGE = "range";

        /// <summary>
        /// Name of the sheet containing template definition.
        /// </summary>
        /// <value></value>
        public string MainSheet { get; set; }
        /// <summary>
        /// Name of the sheet containing any repeater (loops) specification.
        /// </summary>
        /// <value></value>
        public string RepeaterSpecSheet { get; set; }
        /// <summary>
        /// Used to pad the images if added to sheet.
        /// </summary>
        /// <value></value>
        public int[] ImgPadding { get; set; } = { 5, 5 };
        /// <summary>
        /// Any images inserted into the sheet are scaled to this size.
        /// </summary>
        /// <value></value>
        public int[] ImgSize { get; set; } = { 100, 100 };
        /// <summary>
        /// Helper class to process template expressions.
        /// </summary>
        /// <value></value>
        public TemplateHelper th { get; set; }

        private ExcelPackage pkg;
        private ExcelWorksheet wsOut;
        /// <summary>
        /// Tracks how many rows get added at various locations
        /// when data is populated dyamically into the template.
        /// This is useful for calculating row offsets for new
        /// rows.
        /// </summary>
        /// <typeparam name="int">Row index at which added</typeparam>
        /// <typeparam name="int">No. of rows added</typeparam>
        /// <returns></returns>
        private Dictionary<int, int> RowsAddedAt { get; set; } = new Dictionary<int, int>();
        /// <summary>
        /// Tracks how many columns get added at various locations
        /// when data is populated dyamically into the template.
        /// This is useful for calculating column offsets for new
        /// columns.
        /// </summary>
        /// <typeparam name="int">Column index at which added</typeparam>
        /// <typeparam name="int">No of columns added</typeparam>
        /// <returns></returns>
        private Dictionary<int, int> ColsAddedAt { get; set; } = new Dictionary<int, int>();
        private int RowsInSheet { get; set; } = 0;
        private int ColumnsInSheet { get; set; } = 0;
        private static ILog log = LogManager.GetLogger(typeof(ExcelHelper));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateFile"></param>
        /// <param name="mainSheet"></param>
        /// <param name="repSpecSheet"></param>
        public ExcelHelper(string templateFile, string mainSheet = "Main", string repSpecSheet = "RepeaterSpec")
        {
            MainSheet = mainSheet;
            RepeaterSpecSheet = repSpecSheet;
            pkg = new ExcelPackage(new FileInfo(templateFile));
            log.DebugFormat(@"Initialised with MainSheet '{0}', RepeaterSpec '{1}' and template file '{2}'.", mainSheet, repSpecSheet, templateFile);
            th = TemplateHelper.ForIehPeh();
        }

        /// <summary>
        /// We process the repeater expressions sheet to extract
        /// all the repeater spec expressions. While doing so we also
        /// determine how many rows/columns will be added in the main
        /// sheet when we populate the data as per the repeater expressions.
        /// </summary>
        /// <param name="data">The data supplied to us for populating to 
        /// the template's main sheet.</param>
        /// <returns></returns>
        protected RepeatExp GetRepeaterExpInfo(object data)
        {
            log.DebugFormat("======== Processing repeater specs in sheet {0}.", RepeaterSpecSheet);
            RepeatExp re = new RepeatExp();
            var ws = pkg.Workbook.Worksheets[RepeaterSpecSheet];
            for (int i = 1; i <= ws.Dimension.Rows; i++)
            {
                string prop = ws.GetValue<string>(i, 1);
                string type = ws.GetValue<string>(i, 2);
                string range = ws.GetValue<string>(i, 3);
                if (string.IsNullOrEmpty(prop) || string.IsNullOrEmpty(type) ||
                string.IsNullOrEmpty(range)) continue;
                Dictionary<string, string> repInfo = new Dictionary<string, string>();
                repInfo.Add(KEY_PROP, prop);
                repInfo.Add(KEY_TYPE, type);
                repInfo.Add(KEY_RANGE, range);
                re.Exp.Add(repInfo);

                int rc = ws.Cells[range].Rows;
                int cc = ws.Cells[range].Columns;
                TemplateResult tr = th.ProcessExpression(prop, data);
                log.DebugFormat("Checking range size for repeater for range {0}.", range);
                if (tr == null || tr.Data.Count == 0)
                {
                    log.DebugFormat("Skipping empty data for {0}.", prop);
                    continue;
                }
                IList dataList = tr.Data;
                if (dataList.Count > 0)
                {
                    if (ROW.Equals(type, StringComparison.CurrentCultureIgnoreCase))
                    {
                        int ra = rc * (dataList.Count - 1);
                        re.RowsAdded += ra;
                        IncrementItem(RowsAddedAt, ws.Cells[range].Start.Row, ra);
                    }
                    else if (COL.Equals(type, StringComparison.CurrentCultureIgnoreCase))
                    {
                        int ca = cc * (dataList.Count - 1);
                        re.ColumnsAdded += ca;
                        IncrementItem(ColsAddedAt, ws.Cells[range].Start.Column, ca);
                    }
                }
                else
                {
                    // if (ROW.Equals(type, StringComparison.CurrentCultureIgnoreCase))
                    // {
                    //     re.RowsAdded -= rc;
                    // }
                    // else if (COL.Equals(type, StringComparison.CurrentCultureIgnoreCase))
                    // {
                    //     re.ColumnsAdded -= cc;
                    // }
                }
            }
            log.DebugFormat("Repeater expressions {0}.", re);
            return re;
        }

        private void IncrementItem(Dictionary<int, int> map, int row, int count)
        {
            int val = map.GetValueOrDefault(row);
            map[row] = Math.Max(count, val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        private int[] AdjustAddresses(ExcelRange range)
        {
            int[] start = GetDestinationAddr(range.Start.Row, range.Start.Column);
            int[] end = GetDestinationAddr(range.End.Row, range.End.Column);
            log.DebugFormat("Range {0} moved to ({1}, {2}):({3}, {4}).", range.Address, start[0], start[1], end[0], end[1]);
            return new int[] { start[0], start[1], end[0], end[1] };
        }

        /// <summary>
        /// Populates the data from given object into the current template
        /// Excel sheet. Populated Excel package is written to the supplied
        /// MemoryStream. The stream is rewound after writing to it.
        /// </summary>
        /// <param name="data">Data to populate in Excel sheet.</param>
        /// <param name="outStream">Populated Excel package is written to 
        /// this stream.</param>
        public void PopulateSheet(object data, MemoryStream outStream)
        {
            DoPopulateSheet(data, outStream);
            // MUST rewind the stream position!!
            outStream.Position = 0;
        }

        /// <summary>
        /// Populates the data from given object into the current template
        /// Excel sheet. Populated Excel package is written to a file at
        /// supplied path.
        /// </summary>
        /// <param name="data">Data to be populated.</param>
        /// <param name="outFile">Location of output Excel file.</param>
        public void PopulateSheet(object data, string outFile)
        {
            DoPopulateSheet(data, outFile);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="outFile"></param>
        private void DoPopulateSheet(object data, object outFile)
        {
            ExcelWorksheet mainWs = pkg.Workbook.Worksheets[MainSheet];
            RepeatExp re = GetRepeaterExpInfo(data);
            int[] sheetDim = { mainWs.Dimension.Rows + re.RowsAdded,
            mainWs.Dimension.Columns + re.ColumnsAdded };
            // Create the new sheet of required size
            ExcelPackage outPkg = PrepareOutExcelPackage(outFile);
            wsOut = outPkg.Workbook.Worksheets.Add(NEW_DATA_SHEET);
            wsOut.InsertRow(1, sheetDim[0]);
            wsOut.InsertColumn(1, sheetDim[1]);
            log.DebugFormat("========= Processing main sheet {0}.", MainSheet);
            log.DebugFormat("New sheet has {0} rows and {1} columns.", sheetDim[0], sheetDim[1]);
            // Copy non-repeat
            ProcessCells(mainWs, data);
            // Then process the repeaters
            ProcessRepeaters(mainWs, re, data);
            outPkg.Save();
            log.DebugFormat("Saved the sheet at {0}.", outFile);
        }

        /// <summary>
        /// Prepares the Excel package based on the output destination type.
        /// </summary>
        /// <param name="dest">Can be only <see cref="MemoryStream"/> or
        /// a string (holding path of the output file). </param>
        /// <returns></returns>
        private ExcelPackage PrepareOutExcelPackage(object dest)
        {
            if (dest.GetType().Equals(typeof(MemoryStream)))
            {
                log.DebugFormat("Created ExcelPackage using MemoryStream.");
                return new ExcelPackage((MemoryStream)dest);
            }
            else if (dest.GetType().Equals(typeof(string)))
            {
                string outFile = (string)dest;
                if (File.Exists(outFile))
                {
                    File.Move(outFile, outFile + ".old");
                    log.DebugFormat("Renamed the existing file {0}.", outFile);
                }
                log.DebugFormat("Created ExcelPackage using FileInfo.");
                return new ExcelPackage(new FileInfo(outFile));
            }
            else
            {
                throw new NotSupportedException(String.Format(
                    "Destination type '{0}' not supported.", dest));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainWs"></param>
        /// <param name="re"></param>
        /// <param name="data"></param>
        private void ProcessRepeaters(ExcelWorksheet mainWs,
        RepeatExp re, object data)
        {
            log.DebugFormat("Processing repeater cells in main sheet.");
            foreach (var exp in re.Exp)
            {
                string rangeAddr = exp.GetValueOrDefault(KEY_RANGE);
                string dataProp = exp.GetValueOrDefault(KEY_PROP);
                log.DebugFormat("Processing repeater for expression {0} in range {1}.", dataProp, rangeAddr);
                TemplateResult tr = EvaluateProperty(dataProp, data);
                if (tr == null || tr.Data.Count == 0)
                {
                    log.DebugFormat("Skipping due to empty data for {0}.", dataProp);
                    continue;
                }
                ExcelRange range = mainWs.Cells[rangeAddr];
                int[] addr = AdjustAddresses(range);
                int startRow = addr[0];
                int startCol = addr[1];
                int endRow = addr[2];
                int endCol = addr[3];

                IList itemData = tr.Data;
                for (int i = 0; i < itemData.Count; i++)
                {
                    range.Copy(wsOut.Cells[startRow, startCol]);
                    FillDataInCopiedCells(i, itemData[i], wsOut.Cells[startRow, startCol, endRow, endCol]);
                    log.DebugFormat("Copied range {0} to ({1},{2})",
                    rangeAddr, startRow, startCol);
                    if (ROW.Equals(exp.GetValueOrDefault(KEY_TYPE)))
                    {
                        startRow += range.Rows;
                        endRow += range.Rows;
                    }
                    else
                    {
                        startCol += range.Columns;
                        endCol += range.Columns;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainWs"></param>
        /// <param name="data"></param>
        private void ProcessCells(ExcelWorksheet mainWs, object data)
        {
            int rows = mainWs.Dimension.Rows;
            int cols = mainWs.Dimension.Columns;
            log.DebugFormat("Processing normal cells in main sheet. Size: [{0},{1}].", rows, cols);
            for (int r = 1; r <= rows; r++)
            {
                for (int c = 1; c <= cols; c++)
                {
                    int[] d = GetDestinationAddr(r, c);
                    string cellText = mainWs.Cells[r, c].Text;
                    // Skip cells corresponding to repeater items
                    if (cellText.StartsWith("@:")) continue;
                    mainWs.Cells[r, c].Copy(wsOut.Cells[d[0], d[1]]);
                    TemplateResult tr = EvaluateProperty(cellText, data);
                    if (tr != null && tr.Data.Count == 1)
                    {
                        if (tr.HasImage)
                        {
                            string name = string.Format("Picture-{0}-{1}", r, c);
                            AddImage(name, (string)tr.Data[0], d);
                            log.DebugFormat("Added image at ({0},{1}).", d[0], d[1]);
                        }
                        else
                        {
                            wsOut.SetValue(d[0], d[1], GetDataValue(tr.Data[0]));
                        }
                    }
                    else if (!string.IsNullOrEmpty(cellText))
                    {
                        wsOut.SetValue(d[0], d[1], cellText);
                    }
                }
            }
        }

        protected virtual TemplateResult EvaluateProperty(string cellText, object data)
        {
            log.DebugFormat("Evaluating {0}.", cellText);
            return th.ProcessExpression(cellText, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="r"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        private int[] GetDestinationAddr(int r, int c)
        {
            int[] dest = new int[] { r /* Row */, c /* Column */};
            int shiftBy = 0;
            foreach (var row in RowsAddedAt)
            {
                if (r > row.Key)
                {
                    shiftBy += row.Value;
                }
            }
            dest[0] += shiftBy;
            shiftBy = 0;
            foreach (var col in ColsAddedAt)
            {
                if (c > col.Key)
                {
                    shiftBy += col.Value;
                }
            }
            dest[1] += shiftBy;
            return dest;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="dataItem"></param>
        /// <param name="copiedCells"></param>
        private void FillDataInCopiedCells(int index, object dataItem, ExcelRange copiedCells)
        {
            foreach (var cell in copiedCells)
            {
                TemplateResult tr = EvaluateProperty(cell.Text, dataItem);
                if (tr != null && tr.Data.Count == 1)
                {
                    if (tr.HasImage)
                    {
                        AddImage("Picture-" + cell.Address,
                        (string)tr.Data[0], new int[] {cell.Start.Row,
                        cell.Start.Column});
                    }
                    else
                    {
                        cell.Value = GetDataValue(tr.Data[0]);
                    }
                }
                else if (!string.IsNullOrEmpty(cell.Text) && cell.Text.Contains("$index"))
                {
                    cell.Value = cell.Text.Replace("$index", "" + (index + 1));
                }
            }
        }

        private void AddImage(string name, string imgData, int[] rowCol)
        {
            if (string.IsNullOrEmpty(imgData))
            {
                log.WarnFormat("Image data ({0}) is empty. Ignoring.", name);
                return;
            }
            var picture = wsOut.Drawings.AddPicture(name, LoadImageFromBase64(imgData));
            picture.SetPosition(rowCol[0], 5, rowCol[1], 5);
            picture.SetSize(100, 100);
            log.DebugFormat("Added image at ({0},{1}).", rowCol[0], rowCol[1]);
        }

        private object GetDataValue(object obj)
        {
            if (obj == null) return obj;
            if (typeof(DateTime).Equals(obj.GetType()))
            {
                return obj.ToString();
            }
            return obj;
        }

        public void Dispose()
        {
            if (pkg != null) pkg.Dispose();
        }

        public static Image LoadImageFromBase64(string b64str)
        {
            //data:image/gif;base64,
            //data:image/png;base64,
            string mark = ";base64,";
            int ind = b64str.IndexOf(mark) + mark.Length;
            string ss = b64str.Substring(ind);
            log.DebugFormat("Mark index={0}. Original string '{1}', changed: '{2}'.",
            ind, b64str.Substring(0, 24), ss.Substring(0, 24));
            byte[] bytes = Convert.FromBase64String(ss);
            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return image;
        }
    }
}