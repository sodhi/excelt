using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using log4net;

namespace io.github.bsodhi.ExcelUtils
{
    /// <summary>
    /// Types of template expressions.
    /// </summary>
    public enum ExpressionType
    {
        Property = 1, ColRepeat, RowRepeat, Indexer
    }

    /// <summary>
    /// Holder for template expression results.
    /// @author Balwinder Sodhi
    /// </summary>
    public class TemplateResult
    {
        public ExpressionType ExpType { get; set; }
        /// <summary>
        /// Contents of this list are the parsed components of
        /// an a template expression as follows:
        /// Repeater: [0: Expression, 1: Type, 2: Range]
        /// Indexer: [0: Pre-index sub-expression, 
        /// 1: Indexer expression, 2: Post indexer subexpression] 
        /// Simple: [0: Expression]
        /// </summary>
        /// <returns></returns>
        public IList<string> ParsedExp { get; set; }
        /// <summary>
        /// Data values obtained from input object as per the
        /// parsed template expression.
        /// </summary>
        /// <returns></returns>
        public IList Data { get; set; }

        /// <summary>
        /// Index of the data item in case of collection type data value.
        /// </summary>
        /// <returns></returns>
        public int? ItemIndex { get; set; }

        public bool HasImage { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("(ExpType: '{0}'. ParsedExp: '{1}'", ExpType.ToString(), string.Join(',', ParsedExp));
            if (Data != null) sb.AppendFormat(". Data size: '{0}'", Data.Count);
            if (ItemIndex.HasValue) sb.AppendFormat(". ItemIndex: '{0}'", ItemIndex);
            sb.Append(")");
            return sb.ToString();
        }
    }

    /// <summary>
    /// Helper class to handle Excel template expressions. Supported
    /// expression syntax is as follows:
    /// i) Repeater expression:
    /// It should be specified in the "repeater spec" sheet in the
    /// template Excel file. The format of the repeater specs is:
    /// Column 1: Property expression holding the collection to iterate over. Examples: 
    /// a) Cart.Items[Type=ABC] (Repeat for all elements x of collection
    /// Cart.Items where x.Type='ABC')
    /// b) Cart.Items (Repeat for all elements x of collection
    /// Cart.Items)
    /// Column 2: A value of 'row' or 'col' indicating whether to
    /// add repeating rows or columns for each item in the collection.
    /// Column 3: Excel range address in the main sheet which will be 
    /// treated as a template for repeating items. 
    /// Examples: 
    /// a) B10:S10 (Uses cells from a single row, i.e. 10th row)
    /// b) A1:C3 (Uses cells from a region of cells)
    /// 
    /// ii) Simple expression:
    /// Example: #:ShippingAddress.City will return the value of this property
    /// from the input data object.
    /// 
    /// iii) Indexer expression:
    /// Example: #:Order.Item[Id=901]
    /// 
    /// NOTE: If the input object is of type IDictionary<string, object> then
    /// the property names will be treated as keys of the dictionary to access
    /// data value corresponding to it. For example, the expressions
    /// Order.TotalAmt is actually treated as Order["TotalAmt"].
    /// 
    /// @author Balwinder Sodhi
    /// </summary>
    public class TemplateHelper
    {
        private static ILog log = LogManager.GetLogger(typeof(TemplateHelper));

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="ExpressionHandler"></typeparam>
        /// <returns></returns>
        public IList<ExpressionHandler> ExpressionHandlers { get; } = new List<ExpressionHandler>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        public void AddHandler(ExpressionHandler handler)
        {
            ExpressionHandlers.Add(handler);
        }

        /// <summary>
        /// Adds two ExpressionHandlers in that order:
        /// 1) IndexerExpHandler
        /// 2) PropertyExpHandler
        /// </summary>
        public static TemplateHelper ForIehPeh()
        {
            TemplateHelper th = new TemplateHelper();
            // Order of adding handler is important!
            ExpressionHandler eh = new IndexerExpHandler(@"(#:|@:)?(?<img>img:)?(?<p1>((\w+)\.?)+)\[(?<p2>((\w+)\.?)*)\s*\=\s*(?<p2val>((.+)\s?)+)+\]\.?(?<p3>((\w+)\.?)*)");
            th.AddHandler(eh);

            eh = new PropertyExpHandler(@"(#:|@:)(?<img>img:)?(?<path>(?>(\w+)(\.)?)*)");
            th.AddHandler(eh);
            return th;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public TemplateResult ProcessExpression(string expression, object data)
        {
            foreach (var eh in ExpressionHandlers)
            {
                IList<string> p = eh.Parse(expression);
                if (p.Count > 0)
                {
                    log.DebugFormat("Found match for input {0}.", expression);
                    return eh.Handle(expression, p, data);
                }
            }
            log.DebugFormat("No match found for input {0}.", expression);
            return null;
        }

    }

    /// <summary>
    /// To be subclassed for implementing specific types of
    /// expression handlers.
    /// </summary>
    public abstract class ExpressionHandler
    {
        protected ILog log = null;
        protected const string IMG = "img:";
        public ExpressionHandler(string regex)
        {
            Re = new Regex(regex, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            log = LogManager.GetLogger(GetType());
        }
        public Regex Re { get; }
        /// <summary>
        /// Parses the expression in order to return relevant
        /// parts of the expression which will be used for
        /// the template processing.
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        public abstract IList<string> Parse(string exp);
        /// <summary>
        /// Processes the given expression.
        /// </summary>
        /// <param name="expText"></param>
        /// <param name="partsOfExp"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract TemplateResult Handle(string expText, IList<string> partsOfExp, object data);
    }

    public class IndexerExpHandler : ExpressionHandler
    {
        public IndexerExpHandler(string regex) : base(regex)
        {
        }

        public override TemplateResult Handle(string expText, IList<string> partsOfExp, object data)
        {
            // Get data for indexer expression
            log.DebugFormat("Processing indexer expression {0}.", expText);
            var indexedPropValue = data.PropertyValue(partsOfExp[0]);
            IList filtered = new List<object>();
            TemplateResult mr = new TemplateResult() { ParsedExp = partsOfExp, Data = filtered, ExpType = ExpressionType.Indexer };
            mr.HasImage = IMG.Equals(partsOfExp[4]);
            if (indexedPropValue == null)
            {
                log.WarnFormat("Found null value for property for '{0}'. Input expression {1}", partsOfExp[0], expText);
                return mr;
            }
            if (indexedPropValue != null && typeof(IList).IsAssignableFrom(indexedPropValue.GetType()))
            {
                foreach (var item in (IList)indexedPropValue)
                {
                    var obj = item.PropertyValue(partsOfExp[1]);
                    if (obj != null && obj.ToString() == partsOfExp[2])
                    {
                        if (string.IsNullOrEmpty(partsOfExp[3]))
                            filtered.Add(item);
                        else
                            filtered.Add(item.PropertyValue(partsOfExp[3]));
                    }
                }
            }
            else
            {
                log.WarnFormat("Expected collection type property for '{0}', found '{1}'. Input expression {1}", partsOfExp[0], indexedPropValue.GetType(), expText);
            }
            return mr;
        }

        public override IList<string> Parse(string exp)
        {
            Match m = Re.Match(exp);
            IList<string> parts = new List<string>();
            if (m.Success)
            {
                // Order of adding items is important!!
                parts.Add(m.Result("${p1}").Trim());
                parts.Add(m.Result("${p2}").Trim());
                parts.Add(m.Result("${p2val}").Trim());
                parts.Add(m.Result("${p3}").Trim());
                parts.Add(m.Result("${img}").Trim());
            }
            log.DebugFormat("Parsed {0} into {1}.", exp, string.Join(", ", parts));
            return parts;
        }
    }
    public class PropertyExpHandler : ExpressionHandler
    {
        public PropertyExpHandler(string regex) : base(regex)
        {
        }

        public override TemplateResult Handle(string expText, IList<string> partsOfExp, object data)
        {
            log.DebugFormat("Processing property expression {0}.", expText);
            IList val = new List<object>();
            var propVal = Extensions.PropertyValue(data, partsOfExp[0]);
            if (propVal != null && typeof(IList).IsAssignableFrom(propVal.GetType()))
            {
                val = (IList)propVal;
            }
            else
            {
                val.Add(propVal);
            }

            return new TemplateResult()
            {
                ParsedExp = partsOfExp,
                Data = val,
                ExpType = ExpressionType.Property,
                HasImage = IMG.Equals(partsOfExp[1])
            };
        }

        public override IList<string> Parse(string exp)
        {
            Match m = Re.Match(exp);
            IList<string> parts = new List<string>();
            if (m.Success)
            {
                // Order of adding items is important!!
                parts.Add(m.Result("${path}").Trim());
                parts.Add(m.Result("${img}").Trim());
            }
            log.DebugFormat("Parsed {0} into {1}", exp, string.Join(", ", parts));
            return parts;
        }
    }
}