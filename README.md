# excelt

It is a lightweight library for populating Excel based templates with data from C# objects.

## How to install

Source code is available at:

```
$ git clone https://bitbucket.org/sodhi/excelt.git
```

In your .NET Core project, you may include the latest NuGet package which is available at: https://www.nuget.org/packages/ExcelTemplate/

## Features
* Supports populating an Excel file based template with data from arbitrary POCOs.
* Repeater expressions support for handling common cases.
* Output can be written to a file or a MemoryStream.

A simple example of how to use this library can be found in: `https://bitbucket.org/sodhi/excelt/src/master/src/Program.cs`.
For the examples of template files please see the MS Excel files (*.xlsx) under the `https://bitbucket.org/sodhi/excelt/src/master/src/` directory.

## Format of Excel Templates ##

Currently, this library expects the *template* Excel file to contain a main sheet and sheet which contains repeater specs. You may have any number of other sheets in the template Excel file, however, only the contents of the designated main sheet will be populated with the data from an input object. The output Excel file will contain only the main sheet with data populated from supplied object.

Names of the designated *main* and *repeater spec* sheets can be arbitrary and supplied as constructor (shown below) argument of ExcelHelper class.


```
#!c#

public ExcelHelper(string templateFile, string mainSheet = "Main", string repSpecSheet = "RepeaterSpec")
```


### Template Syntax ###

Supported expression syntax is as follows:

#### Repeater expression

It should be specified **only** in the "repeater spec" sheet in the
template Excel file. The "repeater spec" sheet can have more than one rows each containing one repeater expressions of this type. The format of the repeater expression is:

* Column 1: Property expression holding the collection to iterate over. Examples:

1. Cart.Items[Type=ABC] (Repeat for all elements x of collection
Cart.Items where x.Type='ABC')
2. Cart.Items (Repeat for all elements x of collection
Cart.Items)

* Column 2: A value of 'row' or 'col' indicating whether to
add repeating rows or columns for each item in the collection.

* Column 3: Excel range address in the main sheet which will be treated as a template for repeating items.
Examples:

1. B10:S10 (Uses cells from a single row, i.e. 10th row)
2. A1:C3 (Uses cells from a region of cells)

#### Simple expression
These are the property expressions which result in a single (or null/empty) value when evaluated for the input object. This expression can be specified in the main sheet only, and is of two types:

1. Those that are part of a repeated range specified in the "repeater spec" sheet. It contains an input object property expression prefixed with **@:**. Examples: *@:Article.Description*, *Cart.Items[Id=123].Name*
2. Those that are not part of such a repeated range.


Example: #:ShippingAddress.City will return the value of this property
from the input data object.

#### Indexer expression
This expression allows selecting an item or items from a collection, etc. Depending on whether it returns a single or multiple values its use is allowed in the main or repeater spec sheet.
Example: #:Order.Item[Id=901]

NOTE: If the input object is of type IDictionary<string, object> then the property names will be treated as keys of the dictionary to access data value corresponding to it. For example, the expressions Order.TotalAmt is actually treated as Order["TotalAmt"].

Have fun!
